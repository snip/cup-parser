#!/usr/bin/env python3
import csv
import re
import geojson
import json
from geopy.distance import geodesic
#import geo
#import geo.sphere as sphere
#import geo.ellipsoid as ellipsoid
import math

def distance(lat1, lon1, lat2, lon2):
    return geodesic((lat1, lon1), (lat2, lon2)).km
    #return ellipsoid.distance((lat1, lon1), (lat2, lon2))/1000
    #return sphere.distance((lat1, lon1), (lat2, lon2))/1000.0
    #return sphere._haversine_distance((lat1, lon1), (lat2, lon2))/1000.0

def bearing(lat1, lon1, lat2, lon2):
    lat1, lon1, lat2, lon2 = map(math.radians, [lat1, lon1, lat2, lon2])
    dlon = lon2 - lon1
    y = math.sin(dlon) * math.cos(lat2)
    x = math.cos(lat1) * math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(dlon)
    return math.degrees(math.atan2(y, x))+180
    #return sphere.final_bearing((lat1, lon1), (lat2, lon2))

def parse_position(str):
    r = re.compile("([0-9]{2,3})([0-9]{2}\.[0-9]{3})([A-Z])").match(str)
    dd = float(r.group(1)) + float(r.group(2))/60
    if (r.group(3) in ['O', 'W', 'S']):
        dd *= -1
    return dd

def parse_waypoint(row):
    waypoint = {}
    waypoint['name'] = row[0]
    waypoint['code'] = row[1]
    waypoint['country'] = row[2]
    waypoint['latitude'] = parse_position(row[3])
    waypoint['longitude'] = parse_position(row[4])
    waypoint['elevation'] = row[5]
    waypoint['style'] = row[6]
    waypoint['runway_direction'] = row[7]
    waypoint['runway_length'] = row[8]
    waypoint['airport_frequency'] = row[9]
    waypoint['description'] = ','.join(row[10:])
    return waypoint

def parse_options(row):
    options = {}
    for field in row:
        key,val = field.split('=', 1)
        options[key] = val
    return options

cupfile = open('example.cup', 'r', encoding='iso-8859-1')
csvreader = csv.reader(cupfile, delimiter=',', quotechar='"')

step=0
waypoints = {}
tasks = []
task_tmp = {}
for row in csvreader:
    # First line, header "name,code,country,lat,lon,elev,style,rwdir,rwlen,freq,desc"
    if (step == 0):
        step = 1
        if (row[0] == "name"):
            continue

    # Separation line between waypoints part and tasks part
    if (len(row) == 1 and ','.join(row) == "-----Related Tasks-----"):
        step=2
        continue

    # Waypoints
    if (step == 1):
        if (len(row) < 11):
            raise SyntaxError('Waypoint line with less than 11 fields')
        waypoint = parse_waypoint(row)
        waypoints[waypoint['name']] = waypoint

    # Tasks details (the following lines of code must be before "Tasks first line" (step == 2)
    if (step == 3):
        if (row[0] == 'Options'):
            task_tmp['options'] = parse_options(row[1:])
            pass
        elif (row[0].startswith('ObsZone')):
            obs_zone = parse_options(row)
            i = int(obs_zone['ObsZone'])
            task_tmp['turnpoints'][i]['options'] = obs_zone
            
        else:
            tasks.append(task_tmp)
            step = 2

    # Tasks first line
    if (step == 2):
        task_tmp = {}
        task_tmp['description'] = row.pop(0) if (row[0] not in waypoints.keys()) else ""
        #task_tmp['waypoints_names'] = [x for x in row if x != '???']
        task_tmp['turnpoints'] = []
        for wp_name in row:
            if wp_name == '???':
                pass
            elif wp_name in waypoints.keys():
                task_tmp['turnpoints'].append({'name': wp_name})
            else:
                raise SyntaxError('Waypoint "'+wp_name+'" not found in the list of waypoints')
        step = 3

# To finish
tasks.append(task_tmp)

#
print(waypoints)
print(tasks)
print("Names: " + ", ".join(waypoints.keys()) + ".")
print("Waypoints: " + str(len(waypoints)))
print("Tasks: " + str(len(tasks)))

circuits = {}
for task in tasks:
    num = int(task['description'])
    filename = str(num) if num >= 10 else '0' + str(num)
    task_type = "Aller-retour" if len(task['turnpoints']) == 3 else "Triangle" if len(task['turnpoints']) == 4 else "Polygon"
    task_dist = 0.0
    outfile = open('./tasks/task_'+filename+'.geojson', 'w')
    features = []
    prev_point = None
    for tp in task['turnpoints']:
        wp = waypoints[tp['name']]
        p = (wp['longitude'], wp['latitude'])
        if (prev_point is not None):
            d = distance(prev_point[1], prev_point[0], p[1], p[0])
            b = bearing(prev_point[1], prev_point[0], p[1], p[0])
            features.append(geojson.Feature(geometry=geojson.LineString([prev_point, p]), properties={'name': str(round(d, 2)).replace('.', ',') + ' km (' + str(round(b)) + '°)'}))
            task_dist += d
        features.append(geojson.Feature(geometry=geojson.Point(p), properties={'name': tp['name'], 'options': tp['options'], 'infos': wp}))
        prev_point = p
    task_desc = 'Circuit #'+filename+' ('+task_type+' '+str(round(task_dist, 2)).replace('.', ',') + ' km)'
    circuits[task_desc] = filename
    geojson.dump(geojson.FeatureCollection(features), outfile, indent=4, sort_keys=True)
    
        
    #prev_point = None
    #for tp in task['turnpoints']:
    #    wp = waypoints[tp['name']]
    #    p = (wp['longitude'], wp['latitude'])
    #    if (prev_point is not None):
    #        features.append(geojson.Feature(geometry=geojson.LineString([prev_point, p]), properties={}))
    #    features.append(geojson.Feature(geometry=geojson.Point(p), properties={'name': tp['name'], 'options': tp['options'], 'infos': wp}))
    #    prev_point = p
    #geojson.dump(geojson.FeatureCollection(features), outfile, indent=4, sort_keys=True)

print(json.dumps(circuits, indent=4, sort_keys=True))
